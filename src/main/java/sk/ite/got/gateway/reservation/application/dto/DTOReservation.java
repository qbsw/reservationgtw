package sk.ite.got.gateway.reservation.application.dto;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * Created by macalaki on 31.01.2017.
 */
public class DTOReservation {
    public Long id;
    public Long customerId;
    public Long periodId;
    public String note;
    public Date lastModified;
    public Date createAt;
    public Date cancelAt;
    public Integer numberOfGuests = 0;
    public Float totalPrice = 0f;
    public Collection<DTOGuest> guests = new HashSet<DTOGuest>();
}
