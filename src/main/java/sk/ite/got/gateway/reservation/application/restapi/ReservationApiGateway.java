package sk.ite.got.gateway.reservation.application.restapi;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.ite.got.gateway.reservation.application.dto.DTOReservation;
import sk.ite.got.gateway.reservation.application.service.ReservationReader;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "Reservation endpoint")
@RestController
@RequestMapping("/reservation")
class ReservationApiGateway {

	@Autowired
	private ReservationReader reservationReader;

	public String fallback() {
		return "No Reservations";
	}

	@HystrixCommand(fallbackMethod = "fallback")
	@ApiOperation(value = "Create reservation", notes = "Create new reservation for user.")
	@RequestMapping(produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Long createReservation(Long customerId, Long periodId, String note) {
		return reservationReader.createReservation(customerId,periodId,note);
	}

	@HystrixCommand(fallbackMethod = "fallback")
	@ApiOperation(value = "Get reservations", notes = "Get reservations for customer.")
	@RequestMapping(produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<DTOReservation> getAllReservationsForCustomer(Long customerId) {
		return reservationReader.getAllReservationsForCustomer(customerId);
	}

}
