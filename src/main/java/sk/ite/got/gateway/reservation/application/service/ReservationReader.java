package sk.ite.got.gateway.reservation.application.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import sk.ite.got.gateway.reservation.application.dto.DTOReservation;

import java.util.List;

@FeignClient("reservation")
public interface ReservationReader {

	@RequestMapping(method = RequestMethod.POST, value = "/reservation")
    Long createReservation(Long customerId, Long periodId, String note);

	@RequestMapping(method = RequestMethod.GET, value = "/reservation")
	List<DTOReservation> getAllReservationsForCustomer(Long customerId);

}



